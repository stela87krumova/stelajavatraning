package Cars;


public class Car extends Vehicle{
    private  int doors;
    private String model;
    private String motor;
    private int currentGear;
   
    
    public Car(String type, int horsepower, String motorVehicle,int doors, String model,String motor) {
    	super( type, 2002, horsepower, motorVehicle);
    	this.doors=doors;
    	this.model=model;
    	this.motor=motor;
    	this.currentGear=1;
    	
    }
    public void IncreaceVelocity(int currentVelosity) {
    	
    	System.out.println("With velosity you can move");
    }
    public void movingByTires() {
    	System.out.println("Car moves with 4 tires");
    }
    public void changingGears(int currentGear) {
    	this.currentGear=currentGear;
 	   System.out.println("Cars are able to move by changing gears");
 	   System.out.println("Change to " + currentGear + " gear");
    }
	@Override
	public void moving(int speed,String direction) {
		
		super.moving(speed, direction);
		movingByTires();
		System.out.println("Velosity " + speed + " direction " + direction);
	}
	@Override
	public void increaceSpeed() {
		super.increaceSpeed();
		changingGears(2);
		
	}
    
}
