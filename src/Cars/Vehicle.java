package Cars;

public class Vehicle {
   private  String type;
   private int year;
   private int horsepower;
   private String motorVehicle;
   private int  currentDirection;
   private int currentVelocity;
   
   public Vehicle(String type, int year, int horsepower, String motorVehicle) {
	   this.horsepower=horsepower;
	   this.type=type;
	   this.year=year;
	   this.motorVehicle=motorVehicle;
	   this.currentDirection=0;
	   this.currentVelocity=0;
   }
   public void steer(int direction) {
	   this.currentDirection+=direction;
	   System.out.println("Vehicle- steerinng at " + currentDirection + "degrees");
	  
   }
  
   public void increaceSpeed() {
	   System.out.println("Vehicle can increace speed");
   }
   public void decreaceSpeed() {
	   
   }
   public void moving (int speed, String direction) {
	   System.out.println("All vehicle are moves"); 
	   
   }
   public String getType() {
	   return type;
   }
   public int getYear() {
	   return year;
   }
}
