package Inheritance;

public class Dog extends Animal{
	private int eyes;
	private int legs;
	private int tail;
	private int teeth;
	private String coat;
	
   public Dog (String name, int size, int weight,int eyes, int legs, int tail, int teeth,String coat) {
	   super(name,1 ,1,size,weight);
	   this.eyes= eyes;
	   this.coat=coat;
	   this.legs= legs;
	   this.tail= tail;
	   this.teeth= teeth;
   } 
   private void chewFood() {
	   System.out.println("Dog chew food");
   }
@Override
public void eat() {
	System.out.println("Dog.eat");
	chewFood();
	super.eat();
}
   public void walk() {
	   System.out.println("Dog can walk");
	   move(5);
   }
   public void run() {
	   System.out.println("Dog can run");
	   //super.move(20);
	   move(20);
   }
}
