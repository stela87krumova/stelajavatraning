package OOP;

public class BankaAccount implements IRate {
	static final  String routineNumber ="45633";
	String accountNumber;
	private String name;
	String IDNumber;
	String accountType;
	double balance;
	String Msg= null;
	
	//constructor are unique methods
	//- they are used to define/setup/initialize of an object
    //-Constructors are IMLICITLY called upon instantiation
	// MUST have the same name of the class
	//- constructors have NO return type
	
	  BankaAccount(){
		  System.out.println(" New account is created ");
	   }
	  
	  //Overloading- call same method name with different arguments
	 
	  BankaAccount(String accountType){
		  System.out.println(" New account is :  " + accountType );
	  }
	   BankaAccount (String accountType, int intDeposit){
		   System.out.println(" New account is : " + accountType + " and initial deposit is: $" + intDeposit );
		  // String Msg = null;
		   if (intDeposit < 1000) {
			   Msg = " ERROR min deposit must be greater than $1000 ";
			   System.out.println(Msg);
		}else {
			Msg = " Thank you for initial deposit of $" + intDeposit;
			System.out.println(Msg);
			balance = intDeposit;
		}
		  
	   }
		BankaAccount(String accountType,String name, long accountNumber){
			System.out.println(" New account is: " + accountType + " account name is : " + name + " and the number of the same account is : " + accountNumber);
		}
		public void SetRate() {
			System.out.println("Setting rate ");
		}
		public void IncreaseRate() {
			System.out.println("Increasing rate ");
		}
		public void setName( String name) {
			this.name= name;
		}
		public String getName() {
			return name;
		}
		void deposit (double amount) {
			balance = balance + amount;
			System.out.println();
			ShowActivity(" Deposit ");

	}
		void withdraw(double amount) {
			balance = balance + amount;
			ShowActivity(" Withdraw ");
			
		}
		private void ShowActivity( String activity) {
			System.out.println(" Your recent activity is  " + activity);
			System.out.println("Your new balance is " + " $" + balance);
		}
        void status () {
        	System.out.println(" Balance is : " + balance);
        	
        }
        void paybills () {
        	
        }
	
	}

