package exerciseTimbuchalka;

public class MainPersonEx30 {

	public static void main(String[] args) {
		PersonEx30 per = new PersonEx30();
		per.setFirstName("");
		per.setLastName("");
		per.setAge(10);
		System.out.println("FullName = " + per.getFullName());
		System.out.println("Teen = " + per.isTeen());
		per.setFirstName("Stela");
		per.setAge(15);
		System.out.println("FullName = " + per.getFullName());
		System.out.println("Teen = " + per.isTeen());

	}

}
