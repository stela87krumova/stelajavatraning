package exerciseTimbuchalka;

public class CalculatorExerc33 {
   private FloorExerx33 floor;
   private CarpetExerx33 carpet;
   double totalCost;
	public CalculatorExerc33(FloorExerx33 floor, CarpetExerx33 carpet) {
		super();
		this.floor = floor;
		this.carpet = carpet;
	}
	
	public FloorExerx33 getFloor() {
		return floor;
	}

	public CarpetExerx33 getCarpet() {
		return carpet;
	}

	public void setFloor(FloorExerx33 floor) {
		this.floor = floor;
	}

	public void setCarpet(CarpetExerx33 carpet) {
		this.carpet = carpet;
	}

	public double getTotalCost() {
		return carpet.getCoast()*floor.getArea();
		
		
		
	}
    
    
}
