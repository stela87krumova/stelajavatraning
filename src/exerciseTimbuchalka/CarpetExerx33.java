package exerciseTimbuchalka;

public class CarpetExerx33 {
	private double coast;

	public CarpetExerx33(double coast) {
		super();
		this.coast = coast;
	}

	public double getCoast() {
		return coast;
	}

	public void setCoast(double coast) {
		this.coast = coast;
		if (coast<0) {
			this.coast =0.0;
		}
	}
	
	

}
