package ArrayListTimbuchalka;

import java.util.Scanner;

public class MainGroceryList {
	public static Scanner scanner = new Scanner(System.in);
	public static GroceryList groceryList= new GroceryList();

	public static void main(String[] args) {
		boolean quit = false;
		int choice= 0;
		printInstructions();
	    while(!quit) {
	    	System.out.println("Enter your choice:");
	    	choice = scanner.nextInt();
	    	scanner.nextLine();
	    	
	    	switch(choice) {
	    	case 0:
	    		printInstructions();
	    		break;
	    	case 1:
	    		groceryList.printGroceryList();;
	    		break;
	    	case 2:
	    		addItem();
	    		break;
	    	case 3:
	    		modifyItem();
	    		break;
	    	case 4:
	    		removeItem();
	    		break;
	    	case 5:
	    		searchForItem();
	    		break;
	    	case 6:
	    		quit = true;
	    		
	    	}
	    }
		

	}
	public static void printInstructions() {
		System.out.println("Press");
		System.out.println("0 - to print choice option");
		System.out.println("1- to print the list of glocery items");
		System.out.println("2- to add an item to the list");
		System.out.println("3- to modify an item to the list");
		System.out.println("4- to remove an item of the list");
		System.out.println("5- to search an item from the list");
		System.out.println("6- to quit the applicatin");
		
		
	}
	public static void addItem() {
		System.out.print("Pleace enter the grocery item");
		groceryList.addGroceryItem(scanner.nextLine());
	}
	public static void modifyItem() {
		System.out.println("Enter item number:");
		int itemNo = scanner.nextInt();
		scanner.nextLine();
		System.out.println("Enter replacement item");
		String newItem = scanner.nextLine();
		groceryList.modifyGroceryItem(itemNo-1, newItem);
	}
	public static void removeItem() {
		System.out.println("Enter item number:");
		int itemNo = scanner.nextInt();
		scanner.nextLine();
		groceryList.removeGroceryItem(itemNo);
		
	}
	public static void searchForItem() {
		System.out.println("Item for search for: ");
		String searchItem = scanner.nextLine();
		if (groceryList.findItem(searchItem)!= null) {
         System.out.println("Found " + searchItem + " in our grocery list");			
		}else {
			System.out.println("Search Item is not in shopping list ");
		}
		
	}

	}


