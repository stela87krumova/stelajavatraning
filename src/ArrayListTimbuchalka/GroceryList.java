package ArrayListTimbuchalka;

import java.util.ArrayList;

public class GroceryList {
	//private int[] myNumbers=new int[50];
	private ArrayList<String>groceryList= new ArrayList<String>();
	
	public void addGroceryItem(String item) {
		groceryList.add(item);
	}
	
	public void printGroceryList() {
		System.out.println("You have " + groceryList.size());
		for (int i = 0; i < groceryList.size(); i++) {
			System.out.println((i+1)+" " + groceryList.get(i));
		}
	}
	public void modifyGroceryItem(int position, String newItem) {
		groceryList.set(position, newItem);
		System.out.println("grocery item " + (position+1)+ " has been modified" );
	}
	 public void removeGroceryItem(int position) {
		 String theItem= groceryList.remove(position);
	 }
	 public String findItem(String searchItem) {
		//here can use for loop or a contain option- but contain work with boolean primitives, or indexOf-here return -1 if this list does not contain the element
		 //or if contain return the element...!!! 
		// boolean exist = groceryList.contains(searchItem);
		int position = groceryList.indexOf(searchItem);
		if(position>=0) {
			return groceryList.get(position);
		}
		return null;
	 }

	

}
