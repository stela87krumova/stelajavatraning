package interfaceTimbuchalka;

public interface ITelefon {
	//we don't whrite code in interface!!!
	 void powerOn();
	 void dial(int phoneNumber);
	 void answer();
	 boolean callPhone(int phoneNumber);
	 boolean isRinging();

}
