package interfaceTimbuchalka;

public class MainForDeskPhone {

	public static void main(String[] args) {//or we can use DeskPhone stelaPhone = new DeskPhone(4523..); to call the new object
		ITelefon stelaPhone = new DeskPhone(123458);
		stelaPhone.answer();
		stelaPhone.callPhone(123458);
		
		stelaPhone = new MobilePhone(452156);
		stelaPhone.answer();
		stelaPhone.callPhone(452156);
		stelaPhone.powerOn();
		

	}

}
