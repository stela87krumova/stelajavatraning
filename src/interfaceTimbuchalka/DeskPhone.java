package interfaceTimbuchalka;

public class DeskPhone implements ITelefon{
     private int myNumber;
     private boolean isRinging;
     
	public DeskPhone(int myNumber) {
		super();
		this.myNumber = myNumber;
		
	}

	@Override
	public void powerOn() {
		
		
	}

	@Override
	public void dial(int phoneNumber) {
		System.out.println("Now ringing " + phoneNumber);
		
		
	}

	@Override
	public void answer() {
		if (isRinging) {
			System.out.println("Answer");
			isRinging = false;
		}
		
		
	}

	@Override
	public boolean callPhone(int phoneNumber) {
		if (phoneNumber ==myNumber) {
			isRinging= true;
			System.out.println("Ring-Ring");
			
		}else {
			isRinging=false;
		}
		return isRinging;
	}

	@Override
	public boolean isRinging() {
		
		return isRinging;
	}

}
