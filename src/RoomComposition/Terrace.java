package RoomComposition;

public class Terrace {
	private  int area;
	private String view;
	public Terrace(int area, String view) {
		super();
		this.area = area;
		this.view = view;
	}
	public int getArea() {
		return area;
	}
	public String getView() {
		return view;
	}
	
	
	

}
