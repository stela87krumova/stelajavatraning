package RoomComposition;

public class Furniture {
	private int chairs;
	private int table;
	private  int sofa;
	private String TV;
	private int shelf;
	private int curtains;
	private int paintings;
	public Furniture(int chairs, int table, int sofa, String tV, int shelf, int curtains, int paintings) {
		super();
		this.chairs = chairs;
		this.table = table;
		this.sofa = sofa;
		TV = tV;
		this.shelf = shelf;
		this.curtains = curtains;
		this.paintings = paintings;
	}
	public void watchTv(String tvProgram) {
		System.out.println("You watch: " + tvProgram);
	}
	public void readBook(String  theBook) {
		System.out.println("You can read a book - " + theBook );
	}
	public void lightsOn(boolean pressLightButton) {
		if (pressLightButton== true) {
			System.out.println("Lights is on");
			
		}
  
	}
	public int getChairs() {
		return chairs;
	}
	public int getTable() {
		return table;
	}
	public int getSofa() {
		return sofa;
	}
	public String getTV() {
		return TV;
	}
	public int getShelf() {
		return shelf;
	}
	public int getCurtains() {
		return curtains;
	}
	public int getPaintings() {
		return paintings;
	}
	
	
	

}
