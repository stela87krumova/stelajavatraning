package RoomComposition;

public class BasicForRoom {
	private String name;
	 private int area;
	 private int windows;
	 private int doors;
	 private Terrace terrace;
	 private Furniture furniture;
	public BasicForRoom(String name, int area, int windows, int doors, Terrace terrace, Furniture furniture) {
		super();
		this.name = name;
		this.area = area;
		this.windows = windows;
		this.doors = doors;
		this.terrace = terrace;
		this.furniture = furniture;
		
	}
	public void openDoor(String terraceDoor) {
		System.out.println("The door for terrace is open, you can go out");
	}
	public String getName() {
		return name;
	}
	public int getArea() {
		return area;
	}
	public int getWindows() {
		return windows;
	}
	public int getDoors() {
		return doors;
	}
	public Terrace getTerrace() {
		return terrace;
	}
	public Furniture getFurniture() {
		return furniture;
	}
	
	 

}
