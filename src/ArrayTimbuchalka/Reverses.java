package ArrayTimbuchalka;

public class Reverses {

	public static void main(String[] args) {
		String s= "Welcome to Java"; 
		s= reverse(s);
		System.out.println(" after reversing a string is : "+ s);
		}
		

	
	public static String reverse(String s) {
		return new StringBuilder(s).reverse().toString();
	}

}
