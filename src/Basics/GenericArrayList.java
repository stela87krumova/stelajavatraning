package Basics;

import java.util.ArrayList;
import java.util.List;

public class GenericArrayList {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(12);
		list.add(76);
		list.add(null);  // can input null because it is not a type,just value!!!
		//list.add(2.1);- cannot input double
        System.out.println(list);
		
		int i = list.get(0);
		System.out.println(i);
	}

}
