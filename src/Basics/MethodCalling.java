 package Basics;

public class MethodCalling {
	
	//I cannot call the method in main method directly because I have static componenet,but in other methods
	//I have non-static component so I can call it directly(do not have key word- STATIC)
	//static componenet can directly call any static component 
	//non-static->non-static and static -directly
	//static->non-static but when creat an object

	public static void main(String[] args) {
		MethodCalling m = new MethodCalling();
		m.go();
		go3();//static->static-call directly

	}
	public static void go3() {
		System.out.println("inside go3 method");
	}
    public void go() {
    	System.out.println("Inside go Method");
    	go1();
    }
    public void go1() {
    	System.out.println("Inside go Method1");
    	go2();
    }
    public void go2() {
    	System.out.println("Inside go Method2");
    }
}
