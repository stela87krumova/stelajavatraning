package Basics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HashMapExample {

	public static void main(String[] args) {
		Map<String,String> map = new HashMap<String,String>();
		//add values
		map.put("firstName", "Stela");
		map.put("lastName", "Krumova");
		map.put("telefonNumber", "123456258");
		map.put("registrationState", "Sofia");
		map.put("firstName", "Maria");
		
		System.out.println(map);
		System.out.println(map.size());
		System.out.println(map.get("firstName"));
		
		Set<String>keys = map.keySet();  //->������� key �� ����� map, here use SEt  to catch the key because all keys 
		//are unique
		
		for(String key:keys) {
		System.out.println("Key -> " + key + " Value is -> " + map.get(key));	
		}
		
		//��� ����� �������� ���������(����) ��������� ��� 1 key->����. email- stela@gm.com,ivantf@abv.bg,ofissrf@yahoo.com
		// ������ �������� List � ��p
         System.out.println("*********************");
		Map<String,List< String>>map1 = new HashMap<String,List< String>>();
		List<String> listOfEmails = new ArrayList<String>();
        listOfEmails.add("stela87krumova@gmail.com");
		listOfEmails.add("ivantfg@abv.bg");
		listOfEmails.add("offisfthsof@yahoo.com");
		listOfEmails.add("tnt1987@gr.com");
		
		map1.put("emails", listOfEmails);
		//Set<String>keys1= map.keySet();
		for( String key1:map1.keySet()) {
			System.out.println("Key is- " + key1);
			System.out.println("For key : " + key1 + " - value are: " + map1.get(key1));
		}
		//map1.forEach("emails", );
		
		
		
		
		
		
	}

}
