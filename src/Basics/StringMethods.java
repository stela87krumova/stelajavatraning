package Basics;

public class StringMethods {

	public static void main(String[] args) {
		String str = "This is the test string";
		String str1 = "Hello";
		String str2 = "Hello";
		String str3= "Welcome";
		String str4= "            Spaces all around        ";
		
		System.out.println(str.length());
        System.out.println(str.charAt(2));
        System.out.println(str.concat(" This is the appended string"));
        System.out.println(str.contains("is"));
        System.out.println(str.startsWith("This"));
        System.out.println(str.startsWith("is"));
        System.out.println(str.endsWith("String"));
        System.out.println(str.endsWith("string"));
        System.out.println(str1.equals(str3));
        System.out.println(str1.equals(str2));
        System.out.println(str.indexOf("t"));
        System.out.println(str.isEmpty());
        System.out.println(str4.trim());
        
	}

}
