package Basics;

public class SpeedConverter {

	private static double MilesPerHour = 0;

	// public static void main(String[] args) {

	// toMilesPerHour ( - 6.5) ;
	// }
	public static long toMilesPerHour(double kilometersPerHour) {
		if (kilometersPerHour < 0) {
			// double Long =kilometersPerHour ;
			return -1;
		}
		return Math.round(kilometersPerHour / 1.609);

	}

	public static void PrintConversion(double kilometersPerHour) {

		if (kilometersPerHour < 0) {
			System.out.println("Invalid value!");

		} else {
			
			MilesPerHour = toMilesPerHour(kilometersPerHour);
		}
		System.out.println(kilometersPerHour + " km/h = " + MilesPerHour + " mi/h= ");
	}
}
