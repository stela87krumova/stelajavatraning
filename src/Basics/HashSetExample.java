package Basics;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetExample {

	public static void main(String[] args) {
		//with set we take an unique index 
		Set<String> set= new HashSet<String>();
        set.add("Stela");
        set.add("Maria");
        set.add("Krasi");
        set.add("Rosi");
        set.add("Milen");
        set.add("Stefka");
        set.add("Stela");// cannot insert duplicate value
        //when we print the index of list is unorder
        //we cannot use get. with set
        System.out.println(set);
        System.out.println(set.size());
        
        // �� ����� �� ���������� for loop �� ��������� �� set
        
        for(String name:set) {
        	System.out.println(name);
        	
        }
        System.out.println("***********");
        Iterator<String> itr = set.iterator();
        while(itr.hasNext()) {
        	String var = itr.next();
        	if (var.equals("Maria")) {
			System.out.println(var);	
			}
        	//System.out.println(itr.next());
        }
	}

}
