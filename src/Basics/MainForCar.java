package Basics;

import java.util.Scanner;

public class MainForCar {

	public static void main(String[] args) {
//		car myFirstCar = new car();
//		car secondCar = new car();
//		myFirstCar.setMake("Skoda");
//		secondCar.setMake("BMW");
//		
//		//myFirstCar.make= "Skoda";
//		myFirstCar.setModel ("Octavia");
//		secondCar.setModel("M3");
//		
//		myFirstCar.sethorsePower(110);
//		secondCar.sethorsePower(200);
//		System.out.println(secondCar.carInfo());
		
		Scanner scanner = new Scanner(System.in);
		int numberOfCars = Integer.parseInt(scanner.nextLine());
		for (int i = 0; i < numberOfCars; i++) {
			car current = new car();
			String[] carParts = scanner.nextLine().split(" ");
			current.setMake(carParts[0]);
			current.setModel(carParts[1]);
			int horsepower = Integer.parseInt(carParts[2]);
			current.sethorsePower(horsepower);
			
			System.out.println(current.carInfo());
		}
		
		

	}

}
