package Basics;

public class Cities {

	public static void main(String[] args) {
		String[] cities = { "Sofia", "Varna", "Plovdiv", "Burgas" };
		System.out.println(cities[0]);
		System.out.println(cities[1]);
		System.out.println(cities[2]);
		System.out.println(cities[3]);

		// *String[] states = new String[5];
		// states[0] = "California";
		// states[1] = "Ohaio";
		// states[2] = "New Jeesey";
		// states[3] = "Texas";
		// states[4] = "Utah";
		// System.out.println(states[3]);

		String[] countries;
		countries = new String[3];
		countries[0] = "USA";
		countries[1] = "UK";
		countries[2] = "Italy";
		System.out.println(countries[2]);

		System.out.println("**************");

		String[] states = new String[5];
		states[0] = "California";
		states[1] = "Ohaio";
		states[2] = "New Jeesey";
		states[3] = "Texas";
		states[4] = "Utah";
		System.out.println(states[3]);
		int i = 0;

		do {
			System.out.println("STATE: " + states[i]);
			i = i + 1;
		} while (i < 5);
		
		
		int n= 0;
		
		
		while (n<=4) {
			System.out.println("States as " + ": " + states[n]);
			n++;
			
			System.out.println("*** PRINTING WITH FOR LOOP ***");
			
			for (int x = 0; x < 5 ; x++) {
				System.out.println("STATS with FOR AS" + " : " + states[x]);
			}
		}
	}
}

