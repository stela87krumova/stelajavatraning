package exceptions;

import java.util.Scanner;

public class PaymentApp  {
	

	public static void main(String[] args) {
		double payment = 0;
		boolean positivePayment= true;
		
		//System.out.print(" Enter the payment amount : ");
	 // Get the amount and test the value
		//Scanner in = new Scanner(System.in);
		//payment = in.nextDouble();
		
		do {
			System.out.print(" Enter the payment amount : ");
			Scanner in = new Scanner(System.in);
		try {
			payment = in.nextDouble();
			if (payment < 0) {
				throw new NegativPaymentExeption(payment) ;
				
				
			}else {
				positivePayment = true;
			}
		} catch (NegativPaymentExeption e) {
			System.out.println( e.toString() );
			System.out.println(" Please try again! ");
			positivePayment = false;
		}
		}while (!positivePayment);
		
      System.out.println("Tahnk you for your payment ! " + payment );
	}

}
