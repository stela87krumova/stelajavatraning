package exceptions;

public class Finally_block {

	public static void main(String[] args) {
		//we use finally in using of try/catch block
   try {
	   //DB connection- successfully
	   //executing some queries- here we have some Occurred 
	   //validating the data and comparing from websites
	   //closing connection
	   int i[] =new int [4];
		i[5]=100;
   }catch(Throwable t){  //Throwable is a superclass is a perent of Error class and Exception 
	  // class it's better to use it in automation because we do not know the type of error actually
	   System.out.println("Error Occurred");
   }finally {
	   System.out.println("close connection to DB");//to use some part of code ofter exception-error we use
	   //finally block
   }
	}

}
