package exceptions;

public class NegativPaymentExeption extends Exception {
	double payment;
	public NegativPaymentExeption( double payment) {
		//System.out.println(" Error : negative payment! ");
		this.payment = payment;
		
	}
    public String toString() {
    	return " ERROR: Cannot use the negative payment ! " + payment;
    }
}
