package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExampleTimbochlka {
	public static void main(String[] args) {
		int result= divide();
		System.out.println(result);
	}
	
	private static int divide() {
		int x,y;
		try {
		 x = getInt();
		 y = getInt();
		
			System.out.println("x is " + x + " , y is " + y);
			return x/y;
		} catch (ArithmeticException e) {
			throw new ArithmeticException("attempt to divide");
			
		}
		
	}
	private static int getInt() {
		Scanner s = new Scanner(System.in);
		System.out.println("Pleace enter an integer");
		while(true) {
		try {
			return s.nextInt();
		} catch (InputMismatchException e){
			s.nextLine();
		
			System.out.println("Invalid! Must be an integer: 0 to 9!");
		}	
		}
//		catch(ArithmeticException e) {
//		    return 0;
//		}
	}

}
