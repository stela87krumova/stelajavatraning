 package exceptions;

public class ArrayException {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("Beginning");
		try {
		int i[] =new int [4];
		i[5]=100;
		}catch(Exception e) {
			System.out.println("Value under array");
			
		}
		System.out.println("Ending");
		
		Thread.sleep(2000);//this is CAUGHT exception. 
		//I have 2 options to make try-catch block or  add throws declaration in my main method
   // but if I try to use try-catch and decide to use one more time Thread.sleep i must make another
		//try-catch block...
	//so it is better to make a declatarion
	}
	

}
