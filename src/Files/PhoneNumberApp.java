package Files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneNumberApp {

	public static void main(String[] args) {
		// 1 read file and izteglq tel nomera
		// 2 Valid phone numb = 10 digest long
		// Area code cannot started with 0 and 9
		// Cannot be 911
        String[] PhoneNums = new String[9];
		String filename = "C:\\Users\\Think\\Desktop\\PhoneNumber.txt";
		File file = new File(filename);
		String phoneNum = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			phoneNum = br.readLine();
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println(" Error, cannot found the file ");

			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(" Error, could not read the file " + filename);
			e.printStackTrace();
		}
		try {
			if (phoneNum.length() != 10) {
				throw new tenDigitsLong(phoneNum);
			}
			if ((phoneNum.substring(0, 1).equals("0"))){
				throw new ValidArea(phoneNum);
			}
			if (phoneNum.substring(1, 3).equals("9") || (phoneNum.substring(1, 2).equals("1"))
					|| (phoneNum.substring(1, 3).equals("1"))) {
				throw new emergencyExtention(phoneNum);
			}
			System.out.println(phoneNum);
		} catch (tenDigitsLong e) {
			System.out.println(" Phonenumber must be min 10 digits ");
			System.out.println(e.toString());

		} catch (ValidArea e) {
			System.out.println(" Invalid phone number area");
			System.out.println(e.toString());
		} catch (emergencyExtention e) {
			System.out.println(" Invalid phonenumber ");
			System.out.println(e.toString());
		}
	}

}

class tenDigitsLong extends Exception {
	String num;

	public tenDigitsLong(String num) {
		this.num = num;
	}

	public String toString() {
		return " Required tenDigitsLong " + num;
	}
}

class ValidArea extends Exception {
	String num;

	public ValidArea(String num) {
		this.num = num;
	}

	public String toString() {
		return " Phonenumber cannot started with 0 or 9 " + num;

	}

}

class emergencyExtention extends Exception {
	String num;

	public emergencyExtention(String num) {
		this.num = num;
	}

	public String toString() {
		return " Phone number cannot be:  " + num;
	}
}