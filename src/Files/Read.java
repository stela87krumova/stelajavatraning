package Files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Read {

	public static void main(String[] args)  {
		String filename = "C:\\Users\\Think\\Desktop\\stela QA\\Read_File.txt";
		String text= null;
		File file = new File(filename);
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			  text =  br.readLine();
			  br.close();
		} catch (FileNotFoundException e) {
			System.out.println(" Eror, the file not found " + filename);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(" Cannot read the data " + filename);
			e.printStackTrace();
		}finally {
			System.out.println(" Finished reading the file " + filename);
		}
		System.out.println(text); 
	} 
	

}
