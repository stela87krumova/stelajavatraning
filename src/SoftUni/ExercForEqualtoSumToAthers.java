package SoftUni;

import java.util.Scanner;

public class ExercForEqualtoSumToAthers {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int counthNumbers = Integer.parseInt(scanner.nextLine());
		int sum= 0;
		int max = Integer.MIN_VALUE;
		//max= sum -max;
		
		for (int i = 1; i <= counthNumbers; i++) {
			int enterNumber = Integer.parseInt(scanner.nextLine());
			sum+=enterNumber;
			if (enterNumber>max) {
				//System.out.println("Max is = " + max);
				max= enterNumber;
			}
			//sum+=enterNumber;
			
		}
		int sumWithoutMax= sum- max;
		if (sumWithoutMax == max) {
			System.out.println("Yes, sum is = " + max);
		}else {
			int sumDifference= Math.abs(max -sumWithoutMax);
			System.out.println("No, diff is = " + sumDifference);
		}

	}

}
