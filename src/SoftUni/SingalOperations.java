package SoftUni;

import java.util.Scanner;

public class SingalOperations {

	public static void main(String[] args) {
		int count = 3;
		char cherac = '*';
		double price = 2.75;
		String name = "Stela";
		
		//Scanner scanner = new Scanner(System.in);
		//count = scanner.nextLine();
		//count = Integer.parseInt(scanner.nextLine());
		//price = Double.parseDouble(scanner.nextLine());
		//name = scanner.nextLine();
		System.out.println( name + " has bought " + count + " beers for " + price);
		System.out.printf("%s has bought %d beers from %.2f%n" , name, count, price);
		//za print:
		//%.2f- zakryglq do 2q znak,pri double stoinosti
		//%n zapochva na nov red
		System.out.println("7/2 = " + 7/2);
		//ako iskame tochni smetki pone 1 ot chislata ni trqbva da e double!
		System.out.println("7/2.0 = " + 7/2.0);
		//Delenie s ostatyk % = izpolzva se za da se opredeli dali chisloto e chetno ili nechetno
		System.out.println("9%3 = " + 7%2);
		
		//Math.round,ceil,floor
		System.out.println(Math.round(3.75));
		System.out.println(Math.ceil(3.75));
		System.out.println(Math.floor(3.75));
	}

}
