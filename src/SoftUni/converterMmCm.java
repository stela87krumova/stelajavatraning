package SoftUni;

import java.util.Scanner;

public class converterMmCm {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
         double number = Double.parseDouble(scanner.nextLine());
         String currentPoint= scanner.nextLine();
         String exitPoint= scanner.nextLine(); 
         double mm = number*10;
         double cm= number*100;
         double m= number/1000;
         
         if (exitPoint.equals("m")) {
        	 System.out.printf("%.3f",m);
			
		}else if (exitPoint.equals("cm")) {
			System.out.printf("%.3f",cm);
			
		}else if (exitPoint.equals("mm")) {
			System.out.printf("%.3f",mm);
		}else {
			System.out.println("Invalid parameters");
		}
			
         }

}
