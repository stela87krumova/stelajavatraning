package SoftUni;

import java.util.Scanner;

public class SmallShop {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter product:");
		String product = scanner.nextLine();
		System.out.println("Enter town:");
		String town = scanner.nextLine();
		System.out.println("Enter quantity:");
		double quantity = Double.parseDouble(scanner.nextLine());
		double sofiaCoffee = 0.50;
		double sofiaWater = 0.80;
		double sofiaBeer = 1.20;
		double sofiaSweets = 1.45;
		double sofiaPeanuts = 1.60;

		double plovdivCoffee = 0.40;
		double plovdivWater = 0.70;
		double plovdivBeer = 1.15;
		double plovdivSweets = 1.30;
		double plovdivPeanuts = 1.50;

		double varnaCoffee = 0.45;
		double varnaWater = 0.70;
		double varnaBeer = 1.10;
		double varnaSweets = 1.35;
		double varnaPeanuts = 1.55;
		if ((town.equals("Sofia")) && (quantity == 1)) {
			switch (product) {
			case "coffee":
				System.out.println(sofiaCoffee);
				break;
			case "water":
				System.out.println("0.80");
				break;
			case "sweets":
				System.out.println("1.45");
				break;
			case "beer":
				System.out.println("1.20");
				break;
			case "peanuts":
				System.out.println("1.60");
				break;

			}
		} else if ((town.equals("Sofia")) && (quantity > 1)) {
			switch (product) {
			case "coffee":
				double sumSofia = quantity * sofiaCoffee;
				System.out.println(sumSofia);

				break;
			case "water":
				sumSofia = quantity * sofiaWater;
				System.out.println(sumSofia);

				break;
			case "sweets":
				sumSofia = quantity * sofiaSweets;
				System.out.println(sumSofia);

				break;
			case "beer":
				sumSofia = quantity * sofiaBeer;
				System.out.println(sumSofia);

				break;
			case "peanuts":
				sumSofia = quantity * sofiaPeanuts;
				System.out.println(sumSofia);

				break;

			}
		}
		if ((town.equals("Plovdiv")) && (quantity == 1)) {
			switch (product) {
			case "coffee":
				System.out.println("0.40");
				break;
			case "water":
				System.out.println("0.70");
				break;
			case "beer":
				System.out.println("1.15");
				break;
			case "sweets":
				System.out.println("1.30");
				break;
			case "peanuts":
				System.out.println("1.50");
				break;
			}
		} else if ((town.equals("Plovdiv")) && (quantity > 1)) {
			switch (product) {
			case "coffee":
				double sumPlovdiv = quantity * plovdivCoffee;
				System.out.println(sumPlovdiv);

				break;
			case "water":
				sumPlovdiv = quantity * plovdivWater;
				System.out.println(sumPlovdiv);

				break;
			case "sweets":
				sumPlovdiv = quantity * plovdivSweets;
				System.out.println(sumPlovdiv);

				break;
			case "beer":
				sumPlovdiv = quantity * plovdivBeer;
				System.out.println(sumPlovdiv);

				break;
			case "peanuts":
				sumPlovdiv = quantity * plovdivPeanuts;
				System.out.println(sumPlovdiv);

				break;

			}
		} if((town.equals("Varna"))&&(quantity==1)){
			 switch(product) {
			 case "coffee":
			 System.out.println(varnaCoffee);
			 break;
			 case "water":
			 System.out.println(varnaWater);
			 break;
			 case "beer":
			 System.out.println(varnaBeer);
			 break;
			 case "sweets":
			 System.out.println(varnaSweets);
			 break;
			 case "peanuts":
			 System.out.println(varnaPeanuts);
			 break;
			 }
			 }else if ((town.equals("Varna"))&&(quantity>1)) {
			 switch(product) {
			 case "coffee":
				 double sumVarna = quantity*varnaCoffee;
			 System.out.println(sumVarna);
			 
			 break;
			 case "water":
				 sumVarna = quantity*varnaWater;
			 System.out.println(sumVarna);
			
			 break;
			 case "sweets":
				 sumVarna = quantity*varnaSweets;
			 System.out.println(sumVarna);
			 
			 break;
			 case "beer":
				 sumVarna = quantity*varnaBeer;
			 System.out.println(sumVarna);
			 
			 break;
			 case "peanuts":
				 sumVarna = quantity*varnaPeanuts;
			 System.out.println(sumVarna);
			 
			 break;
			
			 }

		}
	}
}
